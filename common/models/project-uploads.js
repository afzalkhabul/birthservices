var server = require('../../server/server');
module.exports = function(Projectuploads) {

  Projectuploads.observe('before save', function (ctx, next) {
    var WorkflowForm=server.models.WorkflowForm;
    var Workflow=server.models.Workflow;
    var WorkflowEmployees=server.models.WorkflowEmployees;
    var notification=server.models.notification;
    if(ctx.instance){
      if(ctx.instance.type!=undefined && ctx.instance.type!=null && ctx.instance.type=='layout' ){
        WorkflowForm.findOne({"where": {"and":[{"status": "Active"},{"schemeUniqueId": "projectLayouts"}]}}, function (err, workflowForm) {
          if(workflowForm!=undefined && workflowForm!=null ){
            Workflow.findOne({'where':{'workflowId':workflowForm.workflowId}},function (err, flowDataList) {
              WorkflowEmployees.find({'where':{'workflowId':workflowForm.workflowId}},function (err, listData) {
                var workflowData = [];
                var employeeDetailsForNotification=[];
                if (listData != undefined && listData != null && listData.length > 0) {
                  for (var i = 0; i < listData.length; i++) {
                    var flowData = {
                      workflowId: listData[i].workflowId,
                      levelId: listData[i].levelId,
                      status: listData[i].status,
                      maxLevels: listData[i].maxLevels,
                      levelNo: listData[i].levelNo

                    }
                    workflowData.push(flowData);
                    if(listData[i].levelNo=="1"){
                      if(listData[i].employees!=undefined && listData[i].employees!=null && listData[i].employees.length>0){
                        for(var j=0;j<listData[i].employees.length;j++){
                          employeeDetailsForNotification.push({'employeeId':listData[i].employees[j],'readStatus':false})
                        }
                      }
                    }
                  }
                }
                ctx.instance.workflow = workflowData;
                ctx.instance.workflowId=workflowForm.workflowId;
                ctx.instance.maxlevel=flowDataList.maxLevel;
                ctx.instance.acceptLevel=0;
                ctx.instance.finalStatus=false;
                ctx.instance.createdTime=new Date();
                ctx.instance.lastUpdateTime=new Date();
                if(employeeDetailsForNotification.length>0){
                  notification.create({
                    'to':employeeDetailsForNotification,
                    "subject":  'New Tasks',
                    "text":"You got New Plan Request",
                    "message":"You got New Plan Request",
                    'urlData':'planLayout',
                    "type":"newRequest"
                  }, function (err, notificationDetails){
                    next();
                  });
                }else{
                  next();
                }
              });

            });
          }
        });

      }else{
       next();
      }
    }
    else{
       next();
    }
  });

  Projectuploads.observe('loaded', function (ctx, next) {
    if(ctx.instance){
      function getWorkflowData() {
        if (ctx.instance.type == 'layout') {
          if (ctx.instance.workflowId != undefined && ctx.instance.workflowId != null && ctx.instance.workflowId != '') {
            var WorkflowEmployees = server.models.WorkflowEmployees;
            WorkflowEmployees.find({'where': {'and': [{'workflowId': ctx.instance.workflowId}, {"status": "Active"}]}}, function (err, workflowEmployeeList) {
              if (workflowEmployeeList != null && workflowEmployeeList.length > 0) {
                ctx.instance.workflowData = workflowEmployeeList;
                next();
              } else {
                next();
              }
            })
          }
          else {
            next();
          }
        } else {
          next();
        }
      }

      if(ctx.instance.documentTypeId){
        var DocumentType = server.models.DocumentType;
        DocumentType.findById(ctx.instance.documentTypeId, function(err, documentInfo){
          if(err){

          }else{
            ctx.instance['documentType'] = documentInfo.name;
          }
          getWorkflowData();
        });
      }else{
        getWorkflowData();
      }
    }else{
      next();
    }
  });

  Projectuploads.getDetails = function (employee,planId, cb) {
    Projectuploads.find({"where":{'and':[{"planId":planId},{'type':'layout'}]}},function(err, fieldVisitData){
      var fieldVisitList=[];
      var adminStatus=false;
      if(fieldVisitData!=null && fieldVisitData.length>0){
       var Employee=server.models.Employee;
        var adminStatus=false;
        Employee.find({'where':{"employeeId":employee}}, function (err, employeeList) {
          if(employeeList!=null && employeeList.length>0) {
            var firstEmployeeDetails = employeeList[0];
            if (firstEmployeeDetails.role == 'superAdmin' || firstEmployeeDetails.role == 'projectAdmin') {
              adminStatus = true;
            }
            for (var i = 0; i < fieldVisitData.length; i++) {
              var data = fieldVisitData[i];
              var workflowDetails = data.workflowData;
              var accessFiledVisit = false;
              var approveFiledVisit = false;
              if (workflowDetails != undefined && workflowDetails != null && workflowDetails.length > 0) {
                var acceptLevel = data.acceptLevel;
                var editDetails;
                if (data.createdPersonId == employee) {
                  editDetails = true;
                }
                var availableStatus = false;
                if (data.finalStatus == false) {
                  for (var j = 0; j < workflowDetails.length; j++) {
                    var availableStatusLevel = false;
                    if (workflowDetails[j].employees != undefined && workflowDetails[j].employees != null && workflowDetails[j].employees.length > 0) {


                      var employeeList = workflowDetails[j].employees;
                      if (employeeList != undefined && employeeList != null && employeeList.length > 0) {
                        for (var x = 0; x < employeeList.length; x++) {
                          if (employeeList[x] == employee) {
                            availableStatus = true;
                            availableStatusLevel = true;
                            break;
                          }
                        }
                      }
                      if (availableStatusLevel) {
                        var workFlowLevel = parseInt(workflowDetails[j].levelNo);
                        if (acceptLevel == (workFlowLevel - 1)) {
                          approveFiledVisit = true;
                        }
                      }else if(adminStatus){
                        availableStatus=true;
                        var workFlowLevel = parseInt(workflowDetails[j].levelNo);
                        if (acceptLevel == (workFlowLevel - 1)) {
                          approveFiledVisit = true;
                        }
                      }

                    }

                  }
                  if (availableStatus || editDetails ) {
                    if (editDetails) {
                      if (acceptLevel == 0 || data.finalStatus == 'Rejected') {
                        data.editDetails = true;
                      } else {
                        data.editDetails = false;
                      }
                    }
                    data.approveFiledVisit = approveFiledVisit;
                    data.availableStatus = availableStatus;
                    fieldVisitList.push(data);
                  }
                }else{
                  if (editDetails) {
                    if (acceptLevel == 0 || data.finalStatus == 'Rejected') {
                      data.editDetails = true;
                    } else {
                      data.editDetails = false;
                    }
                  }
                  data.approveFiledVisit = false;
                  data.availableStatus = true;
                  fieldVisitList.push(data);
                }


              }
            }
            cb(null,fieldVisitList);
          }else{
            cb(null,fieldVisitList);
          }
        });
      }
      else{
        cb(null,fieldVisitList);
      }
    });
  };


  Projectuploads.remoteMethod('getDetails', {
    description: "Send Valid EmployeeId",
    returns: {
      arg: 'data',
      type: "object",
      root:true
    },
    accepts: [{arg: 'employeeId', type: 'string', http: {source: 'query'},required:true},
      {arg: 'planId', type: 'string', http: {source: 'query'},required:true}],
    http: {
      path: '/getDetails',
      verb: 'GET'
    }
  });


  Projectuploads.updateDetails = function (approvalDetails, cb) {

    Projectuploads.findOne({'where':{'id':approvalDetails.requestId}},function(err, fieldVisitDetails){

      if(fieldVisitDetails!=undefined && fieldVisitDetails!=null){
        var finalObject={};
        if(fieldVisitDetails.acceptLevel!=undefined && fieldVisitDetails.acceptLevel!=null && fieldVisitDetails.acceptLevel==approvalDetails.acceptLevel) {
          var workflowDetails = fieldVisitDetails.workflow;
          if (workflowDetails != undefined && workflowDetails != null && workflowDetails.length > 0) {
            var workflowData = [];
            for (var i = 0; i < workflowDetails.length; i++) {
              var data = workflowDetails[i];
              var workFlowLevel = parseInt(workflowDetails[i].levelNo);
              if ((approvalDetails.acceptLevel + 1) == workFlowLevel) {
                data.approvalStatus = approvalDetails.requestStatus;
                data.employeeName = approvalDetails.employeeName;
                data.employeeId = approvalDetails.employeeId;
                data.comment = approvalDetails.comment;
                data.date = new Date();
              }
              workflowData.push(data);

            }
            finalObject.workflow = workflowData;
            finalObject.lastUpdateTime=new Date();
          }
          if (approvalDetails.requestStatus == "Approval") {
            if (fieldVisitDetails.maxlevel == (approvalDetails.acceptLevel + 1)) {
              finalObject.finalStatus = approvalDetails.requestStatus;
              finalObject.acceptLevel = approvalDetails.acceptLevel + 1;

            } else {
              finalObject.acceptLevel = approvalDetails.acceptLevel + 1;
            }

          } else if (approvalDetails.requestStatus == "Rejected") {
            finalObject.finalStatus = approvalDetails.requestStatus;
            finalObject.acceptLevel = approvalDetails.acceptLevel + 1;
            finalObject.rejectComment = approvalDetails.comment;
          }
          fieldVisitDetails.updateAttributes(finalObject, function (err, finalData) {
            var Sms = server.models.Sms;
            if (finalObject.finalStatus != undefined && finalObject.finalStatus != null && finalObject.finalStatus == "Rejected") {
              if (finalData.createdPersonId != undefined && finalData.createdPersonId != null && finalData.createdPersonId != '') {
                var Employee = server.models.Employee;
                var EmailTemplete = server.models.EmailTemplete;
                Employee.find({'where': {'employeeId': finalData.createdPersonId}}, function (err, employeeDetails) {
                  if (employeeDetails != null && employeeDetails.length > 0) {
                    EmailTemplete.findOne({"where": {"emailType": "projectWardWorks"}}, function (err, emailTemplete) {
                      if (emailTemplete != null) {
                        var message = emailTemplete.docUploadRejectedMessage + '<br>Reason is ' + approvalDetails.comment;
                        var Dhanbademail = server.models.DhanbadEmail;
                        var notification=server.models.notification;
                        notification.create({
                          'to':[{'employeeId':employeeDetails[0].employeeId,'readStatus':false}],
                          "subject":  'New Tasks',
                          "text":"Your project plan layout is Rejected",
                          "message":"Your project plan layout is Rejected",
                          'urlData':'planLayout',
                          "type":"approvedRequest"
                        }, function (err, notificationDetails){
                        });
                        Dhanbademail.create({
                          from: emailTemplete.emailId,
                          to: employeeDetails[0].email,
                          subject: emailTemplete.docUploadRejectedEmail, // Subject line
                          "text": message
                        }, function (err, email) {

                        });
                        if (emailTemplete.docUploadRejectedSMS && employeeDetails[0].mobile) {
                          var Sms = server.models.Sms;
                          var smsData = {
                            "message": emailTemplete.docUploadRejectedSMS,
                            "mobileNo": employeeDetails[0].mobile,
                            "smsservicetype": "singlemsg"
                          };
                          Sms.create(smsData, function (err, smsInfo) {

                          });
                        }
                      }
                    });
                    cb(null, finalData);
                  } else {
                    cb(null, finalData);
                  }
                });

              } else {
                cb(null, finalData);
              }

            } else if (finalObject.finalStatus != undefined && finalObject.finalStatus != null && finalObject.finalStatus == "Approval") {

              if (finalData.createdPersonId != undefined && finalData.createdPersonId != null && finalData.createdPersonId != '') {
                var Employee = server.models.Employee;
                var EmailTemplete = server.models.EmailTemplete;
                Employee.find({'where': {'employeeId': finalData.createdPersonId}}, function (err, employeeDetails) {
                  if (employeeDetails != null && employeeDetails.length > 0) {
                    EmailTemplete.findOne({"where": {"emailType": "projectWardWorks"}}, function (err, emailTemplete) {
                      if (emailTemplete != null) {
                        var message = emailTemplete.docUploadRequestApprovalMessage + '<br>Reason is ' + approvalDetails.comment;
                        var Dhanbademail = server.models.DhanbadEmail;
                        var notification=server.models.notification;
                        notification.create({
                          'to':[{'employeeId':employeeDetails[0].employeeId,'readStatus':false}],
                          "subject":  'New Tasks',
                          "text":"Your project plan layout is approved",
                          "message":"Your project plan layout is approved",
                          'urlData':'planLayout',
                          "type":"approvedRequest"
                        }, function (err, notificationDetails){
                          console.log('after update new plan request Approved');
                        });  Dhanbademail.create({
                          from: emailTemplete.emailId,
                          to: employeeDetails[0].email,
                          subject: emailTemplete.docUploadRequestApprovalEmail, // Subject line
                          "text": message
                        }, function (err, email) {

                        });

                        if (emailTemplete.docUploadRequestApprovalSMS && employeeDetails[0].mobile) {
                          var Sms = server.models.Sms;
                          var smsData = {
                            "message": emailTemplete.docUploadRequestApprovalSMS,
                            "mobileNo": employeeDetails[0].mobile,
                            "smsservicetype": "singlemsg"
                          };

                          Sms.create(smsData, function (err, smsInfo) {
                          });
                        }
                      }
                    });
                    cb(null, finalData);
                  } else {
                    cb(null, finalData);
                  }
                });
              }
            } else {


              if(finalData.workflowId!=undefined && finalData.workflowId!=null && finalData.workflowId!=''){
                var WorkflowEmployees=server.models.WorkflowEmployees;
                var notification=server.models.notification;
                WorkflowEmployees.find({'where':{'workflowId':finalData.workflowId}},function (err, listData) {
                  var workflowData = [];
                  var employeeDetailsForNotification=[];
                  if (listData != undefined && listData != null && listData.length > 0) {
                    for (var i = 0; i < listData.length; i++) {
                      if(parseInt(listData[i].levelNo)==(finalData.acceptLevel+1)){
                        if(listData[i].employees!=undefined && listData[i].employees!=null && listData[i].employees.length>0){
                          for(var j=0;j<listData[i].employees.length;j++){
                            employeeDetailsForNotification.push({'employeeId':listData[i].employees[j],'readStatus':false})
                          }
                        }
                      }
                    }
                  }
                  if(employeeDetailsForNotification.length>0){
                    notification.create({
                      'to':employeeDetailsForNotification,
                      "subject":  'New Tasks',
                      "text":"You got New Plan Layout Request",
                      "message":"You got New Plan Layout Request",
                      'urlData':'planLayout',
                      "type":"newRequest"
                    }, function (err, notificationDetails){
                      cb(null, finalData);
                    });

                  }else{
                    cb(null, finalData);
                  }
                });
              }else{
                cb(null, finalData);
              }
            }

          });

        }else{
            var error = new Error('This level is already approved. Can you please refresh page');
            error.statusCode = 200;
            cb(error,null);
          }

        }else {
          var error = new Error('Your plan layout details does not exits');
          error.statusCode = 200;
          cb(error,null);
        }

    });
  };

  Projectuploads.remoteMethod('updateDetails', {
    description: "Send Valid EmployeeId",
    returns: {
      arg: 'data',
      type: "object",
      root:true
    },
    accepts: [{arg: 'approval', type: 'object', http: {source: 'body'}}],
    http: {
      path: '/updateDetails',
      verb: 'POST'
    }
  });

  Projectuploads.updateContent= function (updatedDetails, cb) {

    console.log('input data '+JSON.stringify(updatedDetails));
    Projectuploads.findOne({'where':{'id':updatedDetails.id}},function(err, fieldVisitDetails){


      var WorkflowForm=server.models.WorkflowForm;
      var Workflow=server.models.Workflow;
      var WorkflowEmployees=server.models.WorkflowEmployees;
      var notification=server.models.notification;

       WorkflowForm.findOne({"where": {"and":[{"status": "Active"},{"schemeUniqueId": "fieldVisit"}]}}, function (err, workflowForm) {
       if(workflowForm!=undefined && workflowForm!=null ){
       Workflow.findOne({'where':{'workflowId':workflowForm.workflowId}},function (err, flowDataList) {
       WorkflowEmployees.find({'where':{'workflowId':workflowForm.workflowId}},function (err, listData) {
       var workflowData = [];
         var employeeDetailsForNotification=[];
       if (listData != undefined && listData != null && listData.length > 0) {
       for (var i = 0; i < listData.length; i++) {
       var flowData = {
       workflowId: listData[i].workflowId,
       levelId: listData[i].levelId,
       status: listData[i].status,
       maxLevels: listData[i].maxLevels,
       levelNo: listData[i].levelNo

       }
       workflowData.push(flowData);
         if(listData[i].levelNo=="1"){
           if(listData[i].employees!=undefined && listData[i].employees!=null && listData[i].employees.length>0){
             for(var j=0;j<listData[i].employees.length;j++){
               employeeDetailsForNotification.push({'employeeId':listData[i].employees[j],'readStatus':false})
             }
           }
         }
       }
       }
         updatedDetails.workflow = workflowData;
         updatedDetails.workflowId=workflowForm.workflowId;
         updatedDetails.maxlevel=flowDataList.maxLevel;
         updatedDetails.acceptLevel=0;
         updatedDetails.finalStatus=false;
         updatedDetails['updatedTime']=new Date();
         updatedDetails.lastUpdateTime=new Date();
         notification.create({
           'to':employeeDetailsForNotification,
           "subject":  'New Tasks',
           "text":"You got edit Plan Layout Request",
           "message":"You got edit Plan Layout Request",
           'urlData':'planLayout',
           "type":"newRequest"
         }, function (err, notificationDetails){
           console.log('after update new plan request created');
         });

         fieldVisitDetails.updateAttributes(updatedDetails,function(err,data){
          console.log('successfully updated');
         });
         cb(null, fieldVisitDetails);
       });

       });
       }
       });




    });
  };

  Projectuploads.remoteMethod('updateContent', {
    description: "Send Valid EmployeeId",
    returns: {
      arg: 'data',
      type: "object",
      root:true
    },
    accepts: [{arg: 'approval', type: 'object', http: {source: 'body'}}],
    http: {
      path: '/updateContent',
      verb: 'POST'
    }
  });


};
