var server = require('../../server/server');
module.exports = function(Workflowlevel) {
  Workflowlevel.observe('before save', function (ctx, next) {
    var data;
    if(ctx.instance){
      data = ctx.instance;
      Workflowlevel.find({"where":{"workflowId":data.workflowId, "levelNo":data.levelNo}}, function(err, levelData){
        if(err){
          next(err,null);
        }else{
          if(levelData.length > 0){
            var error = new Error('Level Number already exists');
            error.statusCode = 200;
            next(error, null);
            //next('Level Number already exists', null);
          }else{
            Workflowlevel.find({"where":{"workflowId":data.workflowId, "levelId":data.levelId}}, function(err, levelIdInfo){
              if(levelIdInfo.length > 0) {
                var error = new Error('Level Id already exists');
                error.statusCode = 200;
                next(error, null);
              }else{
                next();
              }
            });
          }
        }
      });
    }else{
      data = ctx.data;
      next();
    }

  });
};
